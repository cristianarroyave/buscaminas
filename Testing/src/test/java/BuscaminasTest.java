import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.BaseStubbing;
import org.mockito.stubbing.Stubber;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class BuscaminasTest {

    Buscaminas buscaminas;

    @BeforeEach
    void setUp() {
        buscaminas = new Buscaminas();

    }



    @Test
    void generarMinas() {



    }

    @Test
    void detectarMinas() {


    }

    @Test
    void hayGanador() {
        buscaminas.hayGanador();
        assertFalse(buscaminas.haGanado);
    }

    @Test
    void esFinalizado() {
        buscaminas.seTermino = true;
        assertTrue(buscaminas.seTermino);
    }

    @Test
    void terminadoConVictoria() {
        buscaminas.haGanado = true;
        assertTrue(buscaminas.haGanado);

    }
}