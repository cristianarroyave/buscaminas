public class Buscaminas {
  public String[][] tableroSolucion = new String[5][5];
  public String[][] tableroJuego = new String[5][5];
  public Boolean seTermino = false;
  public Boolean haGanado = false;
  
  private String desconocido = " ? ";
  private String mina = " * ";
  private String vacio = "   ";
  

  public Buscaminas(){
    for(int x = 0; x < tableroSolucion.length; x++){
      for(int y = 0; y < tableroSolucion[0].length; y++){
        if((x == 0 || x == tableroSolucion.length - 1)||(y == 0 || y == tableroSolucion[0].length - 1)){
          tableroSolucion[x][y] = vacio;
          tableroJuego[x][y] = vacio;
        }
        //Añadir desconocidos
        else{
          tableroSolucion[x][y] = desconocido;
          tableroJuego[x][y] = desconocido;
        }
      }
    }
  }

  public static void imprimirTablero(String[][] str){
    for(int x = 1; x < str.length - 1; x++){   
      for(int y = 0; y < str[0].length ; y++){
        //Formats row.
        if(y > 0 && y < str[0].length)
          System.out.print("|");
        else
          System.out.println("");
        
        System.out.print(str[x][y]); //muestra el contenido de una casilla
      }
    }
  }

  public void mostrarSolucion(){
    System.out.println("_______Solucion______");
    imprimirTablero(tableroSolucion);
    System.out.println("\n_____________________");
  }

  public void actualizar(){
    imprimirTablero(tableroJuego);
    System.out.println("");
  }

  public void generarMinas(int n){
    for(int m = 0; m < n; m++){
      while(true){
        int x, y = 0;
        x = (int)(4*Math.random());
        y = (int)(4*Math.random());
        if(x >= 1 && x <= 4){
          if(y >= 1 && y <= 4){
            if(!tableroSolucion[x][y].equals(mina)){
              tableroSolucion[x][y] = mina;
              break;
            }
          }
        }
      }
    }
  }
  
  public String obtenerValorCasilla(int x, int y){
    return tableroSolucion[x][y];
  }
  
//cuenta el número de minas vacias alrededor de la casilla seleccionada
  public void detectarMinas(){
    for(int x = 1; x <= tableroJuego.length - 2; x++){
      for(int y = 1; y <= tableroJuego.length - 2; y++){
        if(tableroSolucion[x][y].equals(vacio) == true){
          int nums = 0;
          for(int i = (x - 1); i <= (x + 1); i++){
            for(int j = (y - 1); j <= (y + 1); j++){
              if(tableroSolucion[i][j].equals(mina) == true)
                nums++;
            }
          }
          tableroJuego[x][y] = " " + nums + " ";
        }
      }
    }
  }
  

  public void seleccionaCasilla(int x, int y){
    if(tableroSolucion[x][y].equals(desconocido) == true){ //Si no havía sido seleccionada previamente, se dejavacia
      seTermino = false;
      tableroJuego[x][y] = vacio;
      tableroSolucion[x][y] = vacio;
    }else if(tableroSolucion[x][y].equals(mina) == true){  //Se ha seleccionado una mina
      seTermino = true;
      haGanado = false;
      System.out.println("Has perdido!");
    }else if(tableroJuego[x][y].equals(vacio) == true && tableroSolucion[x][y].equals(vacio)){
      seTermino = false;
      System.out.println("Esta casilla no es seleccionable!");
    }
  }
  
  public void hayGanador(){
    int casillasPorSeleccionar = 0;
    for(int i = 0; i < tableroSolucion.length; i++){
      for(int j = 0; j < tableroSolucion[0].length; j++){
        if(tableroSolucion[i][j].equals(desconocido) == true)
          casillasPorSeleccionar++;
      }
    }
    if(casillasPorSeleccionar != 0)
      haGanado = false;
    else{
      haGanado = true;
      seTermino = true;
    }
  }

  public Boolean esFinalizado(){
    return seTermino;
  }
  
  public Boolean terminadoConVictoria(){
    return haGanado;
  }
  
}
